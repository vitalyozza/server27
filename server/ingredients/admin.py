from re import I
from django.contrib import admin
# Import Test Models 
from ingredients.models import Category, Ingredient
# Import GIST Service Models
from ingredients.models import Goal, Idea, Step, Task

# Test Models
admin.site.register(Category)
admin.site.register(Ingredient)
# GIST Service
admin.site.register(Goal)
admin.site.register(Idea)
admin.site.register(Step)
admin.site.register(Task)

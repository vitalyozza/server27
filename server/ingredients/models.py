from traceback import StackSummary
from django.db import models

# Create your models here.
class Category(models.Model):
    name = models.TextField(max_length=100)

    def __str__(self):
        return self.name

class Ingredient(models.Model):
    name = models.CharField(max_length=100)
    notes = models.TextField()
    category = models.ForeignKey(
        Category, related_name="ingredients", on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name

class Status(models.Model):
    name = models.CharField(max_length=10)
    body = models.TextField()
    color = models.CharField(max_length=10)

    def __str__(self):
        return self.name

class Goal(models.Model):
    name = models.CharField(max_length=200)
    order = models.IntegerField(default=0)
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

class Idea(models.Model):
    name = models.CharField(max_length=200)
    order = models.IntegerField(default=0)
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    goal = models.ForeignKey(
        Goal, related_name="ideas", on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name

class Step(models.Model):
    name = models.CharField(max_length=200)
    order = models.IntegerField(default=0)
    body = models.TextField()
    priority = models.TextChoices('PriorityType', 'Low Medium High Urgent')
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    idea = models.ForeignKey(
        Idea, related_name="steps", on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name

class Task(models.Model):
    name = models.CharField(max_length=200)
    order = models.IntegerField(default=0)
    body = models.TextField()
    status = models.ForeignKey(
        Status, related_name="tasks", on_delete=models.SET_NULL, null=True
    )
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    step = models.ForeignKey(
        Step, related_name="tasks", on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name
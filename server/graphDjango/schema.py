# General
from datetime import datetime
from email.policy import default
from tkinter.tix import Tree
from typing_extensions import Required
from urllib import request
from pkg_resources import require
from zmq import proxy

# GraphQL
import graphene
from graphene_django import DjangoObjectType
from graphql_auth.schema import UserQuery, MeQuery
from graphql_auth import mutations

# Models
from ingredients.models import Goal, Idea, Step, Task, Status

class GoalType(DjangoObjectType):
    class Meta:
        model = Goal
        fields = "__all__"

class IdeaType(DjangoObjectType):
    class Meta:
        model = Idea
        fields = "__all__"

class StepType(DjangoObjectType):
    class Meta:
        model = Step
        fields = "__all__"

class TaskType(DjangoObjectType):
    class Meta:
        model = Task
        fields = "__all__"

class StatusType(DjangoObjectType):
    class Meta:
        model = Status
        fields = "__all__"

class Query(UserQuery, MeQuery, graphene.ObjectType):
    
    # GOALS
    all_goals = graphene.List(GoalType)
    get_goal_by_id = graphene.Field(GoalType, id=graphene.ID(required=True))

    def resolve_get_goal_by_id(root, info, id):
        return Goal.objects.get(pk=id)

    def resolve_all_goals(root, info):
        return Goal.objects.all()

    # IDEAS
    all_ideas = graphene.List(IdeaType)
    get_idea_by_id = graphene.Field(IdeaType, id=graphene.ID(required=True))
    get_ideas_by_goal = graphene.List(IdeaType, id=graphene.ID(required=True))

    def resolve_get_idea_by_id(root, info, id):
        return Idea.objects.get(pk=id)

    def resolve_all_ideas(root, info):
        return Idea.objects.all()

    def resolve_get_ideas_by_goal(root, info, id):
        current_goal = Goal.objects.get(pk=id)
        return current_goal.ideas.all()

    # STEPS
    all_steps = graphene.List(StepType)
    get_step_by_id = graphene.Field(StepType, id=graphene.ID(required=True))
    get_steps_by_idea = graphene.List(StepType, id=graphene.ID(required=True))

    def resolve_get_step_by_id(root, info, id):
        return Step.objects.get(pk=id)

    def resolve_all_steps(root, info):
        return Step.objects.all()

    def resolve_get_steps_by_idea(root, info, id):
        current_idea = Idea.objects.get(pk=id)
        return current_idea.steps.all()

    # TASKS
    all_tasks = graphene.List(TaskType)
    get_task_by_id = graphene.Field(TaskType, id=graphene.ID(required=True))
    get_tasks_by_step = graphene.List(TaskType, id=graphene.ID(required=True))
    get_tasks_by_status = graphene.List(TaskType, id=graphene.ID(required=True))

    def resolve_get_task_by_id(root, info, id):
        return Task.objects.get(pk=id)

    def resolve_all_tasks(root, info):
        return Task.objects.all()

    def resolve_get_tasks_by_step(root, info, id):
        current_step = Step.objects.get(pk=id)
        return current_step.tasks.all()

    def resolve_get_tasks_by_status(root, info, id):
        current_status = Status.objects.get(pk=id)
        return current_status.tasks.all()

    # STATUSES
    all_statuses = graphene.List(StatusType)

    def resolve_all_statuses(root, info):
        return Status.objects.all()

''' MUTATIONS '''

# AUTH
class AuthMutation(graphene.ObjectType):
    register = mutations.Register.Field()
    verify_account = mutations.VerifyAccount.Field()
    resend_activation_email = mutations.ResendActivationEmail.Field()
    send_password_reset_email = mutations.SendPasswordResetEmail.Field()
    password_reset = mutations.PasswordReset.Field()
    password_set = mutations.PasswordSet.Field() # For passwordless registration
    password_change = mutations.PasswordChange.Field()
    update_account = mutations.UpdateAccount.Field()
    archive_account = mutations.ArchiveAccount.Field()
    delete_account = mutations.DeleteAccount.Field()
    send_secondary_email_activation =  mutations.SendSecondaryEmailActivation.Field()
    verify_secondary_email = mutations.VerifySecondaryEmail.Field()
    swap_emails = mutations.SwapEmails.Field()
    remove_secondary_email = mutations.RemoveSecondaryEmail.Field()
    # django-graphql-jwt inheritances
    token_auth = mutations.ObtainJSONWebToken.Field()
    verify_token = mutations.VerifyToken.Field()
    refresh_token = mutations.RefreshToken.Field()
    revoke_token = mutations.RevokeToken.Field()

# GOALS
class CreateGoal(graphene.Mutation):
    
    class Arguments:
        name = graphene.String(required=True)
        body = graphene.String(required=True)

    goal = graphene.Field(GoalType)

    @classmethod
    def mutate(cls, root, info, name, body):
        goal = Goal()
        goal.name = name
        goal.body = body
        goal.order = 0
        goal.created = datetime.now()
        goal.updated = datetime.now()
        goal.save()
        return CreateGoal(goal=goal)

class UpdateGoal(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)
        name = graphene.String(required=False, default_value=None)
        body = graphene.String(required=False, default_value=None)
        order = graphene.Int(required=False, default_value=None)

    goal = graphene.Field(GoalType)

    @classmethod
    def mutate(cls, root, info, id, name=None, body=None, order=None):
        goal = Goal.objects.get(pk=id)
        if name is not None:
            goal.name = name
        if body is not None:
            goal.body = body
        if order is not None:
            goal.order = order
        goal.updated = datetime.now()
        goal.save()

        return UpdateGoal(goal=goal)

class RemoveGoal(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)

    goal = graphene.Field(GoalType)

    @classmethod
    def mutate(cls, root, info, id):
        goal = Goal.objects.get(pk=id)
        goal.delete()
        goal.id = id
        return RemoveGoal(goal=goal)

# IDEAS
class CreateIdea(graphene.Mutation):

    class Arguments:
        name = graphene.String(required=True)
        body = graphene.String(required=True)
        goal_pk = graphene.ID(required=True)

    idea = graphene.Field(IdeaType)

    @classmethod
    def mutate(cls, root, info, name, body, goal_pk):
        idea = Idea()
        idea.name = name
        idea.body = body
        idea.order = 0
        idea.goal = Goal.objects.get(pk=goal_pk)
        idea.created = datetime.now()
        idea.updated = datetime.now()
        idea.save()

        return CreateIdea(idea=idea)

class UpdateIdea(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)
        name = graphene.String(required=False, default_value=None)
        body = graphene.String(required=False, default_value=None)
        order = graphene.Int(required=False, default_value=None)
        goal_pk = graphene.ID(required=False, default_value=None)

    idea = graphene.Field(IdeaType)

    @classmethod
    def mutate(cls, root, info, id, name=None, body=None, order=None, goal_pk=None):
        idea = Idea.objects.get(pk=id)
        if name is not None:
            idea.name = name
        if body is not None:
            idea.body = body
        if order is not None:
            idea.order = order
        if goal_pk is not None:
            idea.goal = Goal.objects.get(pk=goal_pk)
        idea.updated = datetime.now()
        idea.save()

        return UpdateIdea(idea=idea)

class RemoveIdea(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)

    idea = graphene.Field(IdeaType)

    @classmethod
    def mutate(cls, root, info, id):
        idea = Idea.objects.get(pk=id)
        idea.delete()
        idea.id = id
        return RemoveIdea(idea=idea)

# STEPS
class CreateStep(graphene.Mutation):

    class Arguments:
        name = graphene.String(required=True)
        body = graphene.String(required=True)
        priority = graphene.String(required=True)
        idea_pk = graphene.ID(required=True)

    step = graphene.Field(StepType)

    @classmethod
    def mutate(cls, root, info, name, body, priority, idea_pk):
        step = Step()
        step.name = name
        step.body = body
        step.order = 0
        step.priority = priority
        step.idea = Idea.objects.get(pk=idea_pk)
        step.created = datetime.now()
        step.updated = datetime.now()
        step.save()

        return CreateStep(step=step)

class UpdateStep(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)
        name = graphene.String(required=False, default_value=None)
        body = graphene.String(required=False, default_value=None)
        order = graphene.Int(required=False, default_value=None)
        priority = graphene.String(required=False, default_value=None)
        idea_pk = graphene.ID(required=False, default_value=None)

    step = graphene.Field(StepType)

    @classmethod
    def mutate(cls, root, info, id, name=None, body=None, order=None, priority=None, idea_pk=None):
        step = Step.objects.get(pk=id)
        if name is not None:
            step.name = name
        if body is not None:
            step.body = body
        if order is not None:
            step.order = order
        if priority is not None:
            step.priority = priority
        if idea_pk is not None:
            step.idea = Idea.objects.get(pk=idea_pk)
        step.updated = datetime.now()
        step.save()

        return UpdateStep(step=step)

class RemoveStep(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)

    step = graphene.Field(StepType)

    @classmethod
    def mutate(cls, root, info, id):
        step = Step.objects.get(pk=id)
        step.delete()
        step.id = id
        return RemoveStep(step=step)

# TASKS
class CreateTask(graphene.Mutation):

    class Arguments:
        name = graphene.String(required=True)
        body = graphene.String(required=True)
        status_pk = graphene.ID(required=True)
        step_pk = graphene.ID(required=True)

    task = graphene.Field(TaskType)

    @classmethod
    def mutate(cls, root, info, name, body, status_pk, step_pk):
        task = Task()
        task.name = name
        task.body = body
        task.order = 0
        task.status = Status.objects.get(pk=status_pk)
        task.step = Step.objects.get(pk=step_pk)
        task.created = datetime.now()
        task.updated = datetime.now()
        task.save()

        return CreateTask(task=task)    

class UpdateTask(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)
        name = graphene.String(required=False, default_value=None)
        body = graphene.String(required=False, default_value=None)
        order = graphene.Int(required=False, default_value=None)
        status_pk = graphene.ID(required=False, default_value=None)
        step_pk = graphene.ID(required=False, default_value=None)

    task = graphene.Field(TaskType)

    @classmethod
    def mutate(cls, root, info, id, name=None, body=None, order=None, status_pk=None, step_pk=None):
        task = Task.objects.get(pk=id)
        if name is not None:
            task.name = name
        if body is not None:
            task.body = body
        if order is not None:
            task.order = order
        if status_pk is not None:
            task.status = Status.objects.get(pk=status_pk)
        if step_pk is not None:
            task.step = Step.objects.get(pk=step_pk)
        task.updated = datetime.now()
        task.save()

        return UpdateTask(task=task)

class RemoveTask(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    task = graphene.Field(TaskType)

    @classmethod
    def mutate(cls, root, info, id):
        task = Task.objects.get(pk=id)
        task.delete()
        task.id = id
        return RemoveTask(task=task)

# STATUSES
class CreateStatus(graphene.Mutation):

    class Arguments:
        name = graphene.String(required=True)
        body = graphene.String(required=True)
        color = graphene.String(required=True)

    status = graphene.Field(StatusType)

    @classmethod
    def mutate(cls, root, info, name, body, color):
        status = Status()
        status.name = name
        status.body = body
        status.color = color
        status.save()

        return CreateStatus(status=status)

class UpdateStatus(graphene.Mutation):

    class Arguments:
        id = graphene.ID(required=True)
        name = graphene.String(required=False, default_value=None)
        body = graphene.String(required=False, default_value=None)
        color = graphene.String(required=False, default_value=None)

    status = graphene.Field(StatusType)

    @classmethod
    def mutate(cls, root, info, id, name=None, body=None, color=None):
        status = Status.objects.get(pk=id)
        if name is not None:
            status.name = name
        if body is not None:
            status.body = body
        if color is not None:
            status.color = color
        status.save()

        return UpdateStatus(status=status)

class RemoveStatus(graphene.Mutation):

    class Arguments:
        id = graphene.ID()

    status = graphene.Field(StatusType)

    @classmethod
    def mutate(cls, root, info, id):
        status = Status.objects.get(pk=id)
        status.delete()
        status.id = status
        return RemoveStatus(status=status)

class Mutation(AuthMutation, graphene.ObjectType):

    # GOALS
    create_goal = CreateGoal.Field()
    update_goal = UpdateGoal.Field()
    remove_goal = RemoveGoal.Field()

    # IDEAS
    create_idea = CreateIdea.Field()
    update_idea = UpdateIdea.Field()
    remove_idea = RemoveIdea.Field()

    # STEPS
    create_step = CreateStep.Field()
    update_step = UpdateStep.Field()
    remove_step = RemoveStep.Field()

    # TASKS
    create_task = CreateTask.Field()
    update_task = UpdateTask.Field()
    remove_taks = RemoveTask.Field()

    # STATUSES
    create_status = CreateStatus.Field()
    update_status = UpdateStatus.Field()
    remove_status = RemoveStatus.Field()

schema = graphene.Schema(
    query=Query,
    mutation=Mutation
)
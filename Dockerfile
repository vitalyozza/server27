FROM alpine:latest
LABEL maintainer="vitaly.govor@icloud.com"

# Install Git
RUN apk update
RUN apk add git
RUN apk add zeromq-dev
RUN apk add --no-cache --virtual .build-deps \
    ca-certificates gcc linux-headers \
    zlib-dev python3-tkinter

# Install python/pip
ENV PYTHONUNBUFFERED=1
RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python
RUN python3 -m ensurepip
RUN pip3 install --no-cache --upgrade pip setuptools

# Get the last version of Github Project 
RUN git clone --depth=1 https://gitlab.com/vitalyozza/server27.git